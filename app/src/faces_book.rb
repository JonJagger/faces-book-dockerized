require 'sinatra/base'
require_relative './images'

class FacesBook < Sinatra::Base

  get '/' do
    erb :faces
  end

  get '/shuffle' do
    @filenames = Images.filenames(2018)
    erb :shuffle
  end

end
