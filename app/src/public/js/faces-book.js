$(() => {

  const makeFace = function(filename) {
    return $('<img>', {
      'class':'face',
      'title':filename.substring(0,filename.indexOf('.')),
        'src':`img/2018/${filename}`
     });
  };

  const tipped = function(node) {
    return node.tooltip({
      position: {
        my: 'center bottom-20',
        at: 'center top',
        using: function(position,feedback) {
          $(this).css(position);
          $('<div>')
            .addClass('arrow')
            .addClass(feedback.vertical)
            .addClass(feedback.horizontal)
            .appendTo(this);
        }
      }
    });
  };

  $('#shuffle').click(function() {
    window.location.reload();
  });

  $.ajax({
    url: '/shuffle',
    success: function(data) {
      $(data).find('li').each(function() {
        const filename = $(this).text();
        const face = makeFace(filename);
        $('faces').append(tipped(face));
      });
    }
  });

});
