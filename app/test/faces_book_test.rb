require 'test_base'

class FacesBookTest < TestBase

  def test_get_root
    get '/'
    assert_response ok
  end

  def test_get_shuffle
    get '/shuffle'
    assert_response ok
  end

end
