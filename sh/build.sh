#!/bin/bash
set -e

readonly ROOT_DIR="$( cd "$( dirname "${0}" )" && cd .. && pwd )"
source ${ROOT_DIR}/.env

docker build \
  --build-arg ALPINE_VERSION=${ALPINE_VERSION} \
  --build-arg FACES_BOOK_HOME=${FACES_BOOK_HOME} \
  --tag ${FACES_BOOK_IMAGE} \
    ${ROOT_DIR}/app
