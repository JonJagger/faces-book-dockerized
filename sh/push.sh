#!/bin/bash
set -e

readonly ROOT_DIR="$( cd "$( dirname "${0}" )" && cd .. && pwd )"
source ${ROOT_DIR}/.env

docker push \
  ${DOCKER_REGISTRY_URL}/${FACES_BOOK_IMAGE}
