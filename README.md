# faces-book-dockerized
- Look at a person's face and try to remember their name.
- Hover the cursor over their face.
- Their name will appear.
- Were you right?

![screenshot](/img/faces-book.png)

Try it now using [play with docker](http://labs.play-with-docker.com/)

- login
- click +ADD NEW INSTANCE (on the left hand side)
- in the terminal enter these two commands
```
git clone https://JonJagger@bitbucket.org/JonJagger/faces-book-dockerized.git
./faces-book-dockerized/sh/local_pipe.sh
```
- wait...
- a port 81 link will appear near the top of the page
- click it!

![play with docker](/img/play-with-docker.png)


